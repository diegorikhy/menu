stopScroll = ->
  trapElement = undefined
  scrollableDist = undefined
  trapClassName = 'sc-stop-scroll-enabled'
  trapSelector = '.sc-stop-scroll'

  trapWheel = (e)->
    if !$('body').hasClass(trapClassName)
      return
    else
      curScrollPos = trapElement.scrollTop()
      wheelEvent = e.originalEvent
      dY = wheelEvent.deltaY
      if dY > 0 && curScrollPos >= (scrollableDist-2) ||
         dY < 0 && curScrollPos <= 0
        return false
    return

  $(document)
  .on 'wheel', trapWheel
  .on 'mouseleave', trapSelector, (e)->
    e.stopPropagation()
    $('body').removeClass trapClassName
    return
  .on 'mousemove', trapSelector, (e)->
    e.stopPropagation()
    trapElement = $(this)
    containerHeight = trapElement.outerHeight()
    contentHeight = trapElement[0].scrollHeight
    scrollableDist = contentHeight - containerHeight
    if contentHeight > containerHeight
      $('body').addClass trapClassName
    return

stopScroll()
