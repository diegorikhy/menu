angular.module 'app', [
  'sc.app.helpers'
]

.controller 'IndexCtrl', [
  '$scope', 'Menu', 'Condominios', 'scToggle'
  (s, Menu, Condominios, toogler)->
    s.menu = Menu.lista

    s.cond =
      q: ''
      dropdown: new toogler
        onOpen: ->
          s.cond.q = ''
      lista: Condominios.lista
      ativo: Condominios.lista[69]
      setCondominio: (condominio)->
        item.active = false for item in @lista when item.active
        condominio.active = true
        @ativo = condominio
        @dropdown.close()

    s.toggleMenu = (menu)->
      item.open = false for item in s.menu unless menu.open
      menu.open = !menu.open
]

.factory 'Menu', ->
  lista: [
    {nome: 'Resumo',      link: '#', icone: 'sc-icon-grafico-3', submenu: []}
    {nome: 'Suplimentos', link: '#', icone: 'sc-icon-compras-2', submenu: [
        {nome: 'Compras',            link: '#'}
        {nome: 'Empresas',           link: '#'}
        {nome: 'Autônomos',          link: '#'}
        {nome: 'Categorias',         link: '#'}
        {nome: 'Unidades de Medida', link: '#'}
      ]
    }
    {nome: 'Financeiro', active: true, link: '#', icone: 'sc-icon-dinheiro-3', submenu: [
        {nome: 'Banco',           link: '#'}
        {nome: 'Caixa',           link: '#'}
        {nome: 'Pagamentos',      link: '#'}
        {nome: 'Pagadores',       link: '#'}
        {nome: 'Lançamentos',     link: '#', active: true}
        {nome: 'Plano de Contas', link: '#'}
      ]
    }
    {nome: 'Moradores', link: '#', icone: 'sc-icon-grupo-2',      submenu: []}
    {nome: 'Agenda',    link: '#', icone: 'sc-icon-calendario-1', submenu: []}
    {nome: 'Ações',     link: '#', icone: 'sc-icon-asterisco',    submenu: [
        {nome: 'Entrar no Site',     link: '#'}
        {nome: 'Mural',              link: '#'}
        {nome: 'Votações',           link: '#'}
        {nome: 'Ocorrências',        link: '#'}
        {nome: 'Circulares',         link: '#'}
        {nome: 'Permissões',         link: '#'}
        {nome: 'Estoque/Patrimônio', link: '#'}
        {nome: 'Infrações',          link: '#'}
        {nome: 'Documentos',         link: '#'}
        {nome: 'Tarefas',            link: '#'}
      ]
    }
    {nome: 'Portaria', link: '#', icone: 'sc-icon-executivo', submenu: [
        {nome: 'Visitantes',          link: '#'}
        {nome: 'Passagem de Serviço', link: '#'}
        {nome: 'Telefones Úteis',     link: '#'}
        {nome: 'Empréstimos',         link: '#'}
        {nome: 'Encomendas',          link: '#'}
      ]
    }
  ]

.factory 'Condominios', ->
  lista: [
    { name: 'Administrar Condomínios' }
    { name: 'Adriático' }
    { name: 'Alegria Santa Luzia' }
    { name: 'Âmbar' }
    { name: 'Ambar Stlyle' }
    { name: 'Ametista' }
    { name: 'Amsterdam' }
    { name: 'A Parceria Imóveis' }
    { name: 'Ariadne' }
    { name: 'Athenas' }
    { name: 'Atmosphere Front Park' }
    { name: 'Borges Landeiro Plaza' }
    { name: 'Borges Landeiro Santorini' }
    { name: 'Boulevard' }
    { name: 'Breeze' }
    { name: 'Brisas Do Madeira' }
    { name: 'Brise' }
    { name: 'Cachoeira Bom Sucesso' }
    { name: 'Caldas Novas Ville' }
    { name: 'Caliandra Club' }
    { name: 'Caminho do Mar' }
    { name: 'Campo Bello Residence' }
    { name: 'Casa das Máquinas' }
    { name: 'Castell Di Villa Rica' }
    { name: 'CCI' }
    { name: 'CEMEB' }
    { name: 'Chateau du Parc' }
    { name: 'City Hall' }
    { name: 'Club Cheverny' }
    { name: 'Coliseum Residence' }
    { name: 'Condhor Terceirização' }
    { name: 'Condomínio Águas da Fonte' }
    { name: 'Condomínio Alvorecer' }
    { name: 'Condomínio Amanhecer' }
    { name: 'Condomínio Atenas' }
    { name: 'Condomínio Caminho' }
    { name: 'CONDOMINIO DAS ACACIAS' }
    { name: 'Condomínio dos Pássaros' }
    { name: 'Condomínio Energia' }
    { name: 'Condominio Harmonia' }
    { name: 'Condomínio Hera' }
    { name: 'Condomínio Horizonte' }
    { name: 'Condomínio Íris' }
    { name: 'Condomínio Led' }
    { name: 'Condomínio Lilás' }
    { name: 'Condomínio Lótus' }
    { name: 'Condomínio Momento' }
    { name: 'Condomínio Park Style' }
    { name: 'Condomínio Plenitude' }
    { name: 'CONDOMINIO PORTO FINO' }
    { name: 'Condomínio Praça Jardim' }
    { name: 'Condomínio Realidade' }
    { name: 'Condomínio Taurus' }
    { name: 'Condomínio Turmalinas' }
    { name: 'CONDOMINIO URBAN' }
    { name: 'Condomínio Vaga Fogo' }
    { name: 'Condomínio Vila Rica' }
    { name: 'Condomínio Villa Tropical' }
    { name: 'Condomínio Vivant' }
    { name: 'Conquista Residencial' }
    { name: 'Contart' }
    { name: 'Corelli' }
    { name: 'Cores do Piatã' }
    { name: 'Costa Dourada' }
    { name: 'Crescer' }
    { name: 'Diamond' }
    { name: 'Diamond Residencial Tower' }
    { name: 'Domani Life Style' }
    { name: 'Dona Geninha' }
    { name: 'Edifício Canadá' }
    { name: 'Edifício Georgette' }
    { name: 'Edifício Judith' }
    { name: 'Edifício Kennedy' }
    { name: 'EDIFÍCIO MIDAS' }
    { name: 'Edifício ONE' }
    { name: 'EDIFÍCIO SAINT PAUL' }
    { name: 'Edifício São Jorge' }
    { name: 'Evidence' }
    { name: 'Felicitá' }
    { name: 'Flor do Cerrado' }
    { name: 'Flor do Ipê' }
    { name: 'Fox' }
    { name: 'Gran Campinas' }
    { name: 'Grand Amazônia' }
    { name: 'Green Park Residence' }
    { name: 'Green Valley' }
    { name: 'Happy Days' }
    { name: 'Housing Flamboyant' }
    { name: 'Ícone Residence' }
    { name: 'Ilha Murano' }
    { name: 'Ilha Nuova' }
    { name: 'Imperador do Park' }
    { name: 'Império' }
    { name: 'Interclube II' }
    { name: 'Janaína' }
    { name: 'Jardim da Serra' }
    { name: 'Jardim Dei Fiori' }
    { name: 'JARDIM SALTO GRANDE lll' }
    { name: 'Jardins Residence' }
    { name: 'Jeová Rafá' }
    { name: 'Jerônimo Martins' }
    { name: 'Júbilo' }
    { name: 'La Musique Resort' }
    { name: 'Las Vegas' }
    { name: 'Le Parc' }
    { name: 'Loft Gyn' }
    { name: 'Logos Life' }
    { name: 'Lourenço Residence' }
    { name: 'Luiz Gustavo Tavares' }
    { name: 'Marechal Rondon' }
    { name: 'Marfim' }
    { name: 'Máximo Independence' }
    { name: 'Mirage' }
    { name: 'Mirante do Arvoador' }
    { name: 'Narciso' }
    { name: 'New Park' }
    { name: 'Oeste Tower Residence' }
    { name: 'OLIMPIC CONDOMINIUM' }
    { name: 'Paineiras' }
    { name: 'Palais du Parc' }
    { name: 'Palazzo Di Itália' }
    { name: 'Paraíso Tropical' }
    { name: 'Park House Flamboyant' }
    { name: 'Peônia' }
    { name: 'Point Convenience' }
    { name: 'Pontal dos Tocantins' }
    { name: 'Portal Das Dunas' }
    { name: 'Portal dos Ipês' }
    { name: 'Portal dos Parques' }
    { name: 'PORTO BELO II' }
    { name: 'Porto Seguro' }
    { name: 'Quinta Das Ladeiras' }
    { name: 'Reality Buritis' }
    { name: 'REALITY SÃO LUIZ' }
    { name: 'Reality Vila Maria' }
    { name: 'Recanto Praças', active: true}
    { name: 'Recanto Praças 2' }
    { name: 'Recomeço' }
    { name: 'Renascer' }
    { name: 'Reserva do Rio Grande' }
    { name: 'Reserva Jardim do Parque' }
    { name: 'RESIDENCIAL BONAVITA' }
    { name: 'Residencial Esplendore' }
    { name: 'RESIDENCIAL FELICITÁ' }
    { name: 'Residencial J C Vida' }
    { name: 'Residencial Juracy Rolim' }
    { name: 'RESIDENCIAL LA FONTAINE' }
    { name: 'Residencial Loregiola' }
    { name: 'Residencial Mônaco' }
    { name: 'Residencial Pavonia' }
    { name: 'Residencial Portinari' }
    { name: 'RESIDENCIAL SUBLIME' }
    { name: 'Residencial Tropicale' }
    { name: 'Riviera Di Capri' }
    { name: 'Rosa' }
    { name: 'Saint James Park' }
    { name: 'Saint Louis' }
    { name: 'São Cristóvão' }
    { name: 'Self Seg' }
    { name: 'SERVICES TERCEIRIZACOES' }
    { name: 'Seu Condomínio' }
    { name: 'Sonho' }
    { name: 'Sorte' }
    { name: 'SPAZIO GRAN OLYMPUS' }
    { name: 'Spazio Gran Real' }
    { name: 'Sunset Boulevard' }
    { name: 'Tendence Residencial Club' }
    { name: 'Teste' }
    { name: 'Teste 2' }
    { name: 'Teste 3' }
    { name: 'THE ONE' }
    { name: 'Two Life Residence' }
    { name: 'University Residence' }
    { name: 'Varandas da Praça' }
    { name: 'Vencer' }
    { name: 'Ventana' }
    { name: 'Verbena' }
    { name: 'Verde Vida' }
    { name: 'Veredas do Bueno' }
    { name: 'Victória Prenazzi Viol' }
    { name: 'Vila Rica' }
    { name: 'Village do Parque' }
    { name: 'Village Parati' }
    { name: 'Villagio di Venezia' }
    { name: 'VINGT-TROIS' }
    { name: 'Vinson' }
    { name: 'Virtude' }
    { name: 'Visage Acualle' }
    { name: 'Visionaire' }
    { name: 'Vista Azul - Artista' }
    { name: 'Vista Azul - Corsário' }
    { name: 'Vista Azul - Piatã' }
    { name: 'Vista Azul - Pituaçu' }
    { name: 'Vitória Régia 2' }
    { name: 'Week Family' }
    { name: 'Yes - 2' }
    { name: 'Zeus Park House' }
  ]
